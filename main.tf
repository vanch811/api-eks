module "eks" {
  source = "./modules/eks-modules"

  cluster-name        = local.cluster-name
  k8s-version         = local.k8s-version
  node-instance-type  = local.node-instance-type
  root-block-size     = var.root-block-size
  desired-capacity    = var.desired-capacity
  max-size            = var.max-size
  min-size            = var.min-size
  vpc-subnet-cidr     = local.vpc-subnet-cidr
  private-subnet-cidr = local.private-subnet-cidr
  public-subnet-cidr  = local.public-subnet-cidr
  eks-cw-logging      = var.eks-cw-logging
}