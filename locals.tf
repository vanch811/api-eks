locals {
  environment        = terraform.workspace
  cluster_info       = lookup(var.environment, local.environment)
  cluster-name       = local.cluster_info["cluster-name"]
  k8s-version        = local.cluster_info["k8s-version"]
  node-instance-type = local.cluster_info["node-instance-type"]
  vpc-subnet-cidr    = local.cluster_info["vpc-subnet-cidr"]
  private-subnet-cidr= local.cluster_info["private-subnet-cidr"]
  public-subnet-cidr = local.cluster_info["public-subnet-cidr"]

}