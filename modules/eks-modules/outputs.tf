output "cluster-name" {
    value = aws_eks_cluster.eks.id
}

output "cluster-endpoint" {
    value = aws_eks_cluster.eks.endpoint
}