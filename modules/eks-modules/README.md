Deploy a full AWS EKS cluster with Terraform

What resources are created
VPC
Internet Gateway (IGW)
Public and Private Subnets
Security Groups, Route Tables and Route Table Associations
IAM roles, instance profiles and policies
An EKS Cluster
EKS Managed Node group
Autoscaling group and Launch Configuration
Worker Nodes in a private Subnet

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | 3.0.0 |

## Resources

| Name | Type |
|------|------|
| [aws_eks_cluster.eks](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster) | resource |
| [aws_eks_node_group.eks-node-group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group) | resource |
| [aws_iam_instance_profile.node](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.node](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.cluster-AmazonEKSClusterPolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.cluster-AmazonEKSServicePolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.node-AmazonEC2ContainerRegistryReadOnly](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.node-AmazonEKSWorkerNodePolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.node-AmazonEKS_CNI_Policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_security_group.cluster-sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.node-sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.cluster-sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.node-sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_subnet_ids.private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet_ids) | data source |
| [aws_subnet_ids.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet_ids) | data source |
| [aws_vpc.eks](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_availability-zones"></a> [availability-zones](#input\_availability-zones) | The AWS AZ to deploy EKS | `list(any)` | <pre>[<br>  "ap-south-1a",<br>  "ap-south-1b",<br>  "ap-south-1c"<br>]</pre> | no |
| <a name="input_aws-region"></a> [aws-region](#input\_aws-region) | The AWS Region to deploy EKS | `string` | `"ap-south-1"` | no |
| <a name="input_cluster-name"></a> [cluster-name](#input\_cluster-name) | The name of your EKS Cluster | `string` | `"eks-cluster"` | no |
| <a name="input_desired-capacity"></a> [desired-capacity](#input\_desired-capacity) | Autoscaling Desired node capacity | `string` | `2` | no |
| <a name="input_eks-cw-logging"></a> [eks-cw-logging](#input\_eks-cw-logging) | Enable EKS CWL for EKS components | `list(any)` | <pre>[<br>  "api",<br>  "audit",<br>  "authenticator",<br>  "controllerManager",<br>  "scheduler"<br>]</pre> | no |
| <a name="input_k8s-version"></a> [k8s-version](#input\_k8s-version) | Required K8s version | `string` | `"1.21"` | no |
| <a name="input_kublet-extra-args"></a> [kublet-extra-args](#input\_kublet-extra-args) | Additional arguments to supply to the node kubelet process | `string` | `""` | no |
| <a name="input_max-size"></a> [max-size](#input\_max-size) | Autoscaling maximum node capacity | `string` | `2` | no |
| <a name="input_min-size"></a> [min-size](#input\_min-size) | Autoscaling Minimum node capacity | `string` | `2` | no |
| <a name="input_node-instance-type"></a> [node-instance-type](#input\_node-instance-type) | Worker Node EC2 instance type | `string` | `"m4.large"` | no |
| <a name="input_private-subnet-cidr"></a> [private-subnet-cidr](#input\_private-subnet-cidr) | Private Subnet CIDR | `list(any)` | <pre>[<br>  "10.0.0.0/19",<br>  "10.0.32.0/19",<br>  "10.0.64.0/19"<br>]</pre> | no |
| <a name="input_public-kublet-extra-args"></a> [public-kublet-extra-args](#input\_public-kublet-extra-args) | Additional arguments to supply to the public node kubelet process | `string` | `""` | no |
| <a name="input_public-subnet-cidr"></a> [public-subnet-cidr](#input\_public-subnet-cidr) | Public Subnet CIDR | `list(any)` | <pre>[<br>  "10.0.128.0/20",<br>  "10.0.144.0/20",<br>  "10.0.160.0/20"<br>]</pre> | no |
| <a name="input_root-block-size"></a> [root-block-size](#input\_root-block-size) | Size of the root EBS block device | `string` | `"20"` | no |
| <a name="input_vpc-subnet-cidr"></a> [vpc-subnet-cidr](#input\_vpc-subnet-cidr) | The VPC Subnet CIDR | `string` | `"10.0.0.0/16"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster-endpoint"></a> [cluster-endpoint](#output\_cluster-endpoint) | n/a |
| <a name="output_cluster-name"></a> [cluster-name](#output\_cluster-name) | n/a |
<!-- END_TF_DOCS -->

How to use this example


module "eks" {
  source  = "modules/eks-modules"

  aws-region          = "ap-south-1"
  availability-zones  = ["ap-south-1a", "ap-south-1b", "ap-south-1c"]
  cluster-name        = "my-cluster"
  k8s-version         = "1.17"
  node-instance-type  = "t3.medium"
  root-block-size     = "40"
  desired-capacity    = "3"
  max-size            = "5"
  min-size            = "1"
  vpc-subnet-cidr     = "10.0.0.0/16"
  private-subnet-cidr = ["10.0.0.0/19", "10.0.32.0/19", "10.0.64.0/19"]
  public-subnet-cidr  = ["10.0.128.0/20", "10.0.144.0/20", "10.0.160.0/20"]
  eks-cw-logging      = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
}

output "cluster-name" {
    value = module.eks.cluster-name
}

output "cluster-endpoint" {
    value = module.eks.cluster-endpoint
}

Or by using variables.tf or a tfvars file:

module "eks" {
  source  = "modules/eks-modules"

  aws-region          = var.aws-region
  availability-zones  = var.availability-zones
  cluster-name        = var.cluster-name
  k8s-version         = var.k8s-version
  node-instance-type  = var.node-instance-type
  root-block-size     = var.root-block-size
  desired-capacity    = var.desired-capacity
  max-size            = var.max-size
  min-size            = var.min-size
  vpc-subnet-cidr     = var.vpc-subnet-cidr
  private-subnet-cidr = var.private-subnet-cidr
  public-subnet-cidr  = var.public-subnet-cidr
  eks-cw-logging      = var.eks-cw-logging
}

Terraform
You need to run the following commands to create the resources with Terraform:

terraform init
terraform plan
terraform apply

Cleaning up
You can destroy this cluster entirely by running:

terraform plan -destroy
terraform destroy  --force