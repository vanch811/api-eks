
# CLUSTER SECURITY GROUPS
resource "aws_security_group" "cluster-sg" {
  name        = "cluster-sg"
  description = "EKS cluster security groups"
  vpc_id      = data.aws_vpc.eks.id

  tags = {
    Name = "${var.cluster-name}-eks-cluster-sg"
  }
}

# NODES SECURITY GROUPS

resource "aws_security_group" "node-sg" {
  name        = "node-sg"
  description = "EKS node security groups"
  vpc_id      = data.aws_vpc.eks.id

  tags = {
    Name                                        = "${var.cluster-name}-eks-node-sg"
    "kubernetes.io/cluster/${var.cluster-name}" = "owned"
  }
}

resource "aws_security_group_rule" "cluster-sg" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  source_security_group_id = aws_security_group.node-sg.id
  security_group_id = aws_security_group.cluster-sg.id
}

resource "aws_security_group_rule" "node-sg" {
  type              = "ingress"
  from_port         = 1025
  to_port           = 65535
  protocol          = "tcp"
  source_security_group_id = aws_security_group.cluster-sg.id
  security_group_id = aws_security_group.node-sg.id
}