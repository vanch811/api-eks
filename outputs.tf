output "cluster-name" {
    value = module.eks.cluster-name
}

output "cluster-endpoint" {
    value = module.eks.cluster-endpoint
}