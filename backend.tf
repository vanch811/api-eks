terraform {
  backend "s3" {
    bucket = "chinmayinfra-state-bucket"
    key    = "terraform/state/api-eks"
    region = "ap-south-1"
  }
}