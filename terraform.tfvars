environment = {
  default = {
    cluster-name        = "api-cluster"
    k8s-version         = "1.21"
    node-instance-type  = "t3.micro"
    vpc-subnet-cidr     = "10.10.0.0/16"
    private-subnet-cidr = ["10.10.0.0/19", "10.10.32.0/19", "10.10.64.0/19"]
    public-subnet-cidr  = ["10.10.128.0/20", "10.10.144.0/20", "10.10.160.0/20"]
    eks-cw-logging      = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  }
}