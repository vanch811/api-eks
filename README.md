<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14.7 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_eks"></a> [eks](#module\_eks) | ./modules/eks-modules | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_desired-capacity"></a> [desired-capacity](#input\_desired-capacity) | Autoscaling Desired node capacity | `string` | `2` | no |
| <a name="input_eks-cw-logging"></a> [eks-cw-logging](#input\_eks-cw-logging) | Enable EKS CWL for EKS components | `list(any)` | <pre>[<br>  "api",<br>  "audit",<br>  "authenticator",<br>  "controllerManager",<br>  "scheduler"<br>]</pre> | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Variables Configuration | `map(any)` | n/a | yes |
| <a name="input_kublet-extra-args"></a> [kublet-extra-args](#input\_kublet-extra-args) | Additional arguments to supply to the node kubelet process | `string` | `""` | no |
| <a name="input_max-size"></a> [max-size](#input\_max-size) | Autoscaling maximum node capacity | `string` | `3` | no |
| <a name="input_min-size"></a> [min-size](#input\_min-size) | Autoscaling Minimum node capacity | `string` | `1` | no |
| <a name="input_public-kublet-extra-args"></a> [public-kublet-extra-args](#input\_public-kublet-extra-args) | Additional arguments to supply to the public node kubelet process | `string` | `""` | no |
| <a name="input_root-block-size"></a> [root-block-size](#input\_root-block-size) | Size of the root EBS block device | `string` | `"20"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster-endpoint"></a> [cluster-endpoint](#output\_cluster-endpoint) | n/a |
| <a name="output_cluster-name"></a> [cluster-name](#output\_cluster-name) | n/a |
<!-- END_TF_DOCS -->